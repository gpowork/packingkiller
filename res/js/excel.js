var getSelected = function() {
    var list  = document.getElementsByClassName("title-select");
    var arr = [];
    if (list && list.length) {
        for (var i = 0; i < list.length; i++) {
            arr.push(list[i][list[i].selectedIndex].value);
        }
    }
    return arr.join(",");
}