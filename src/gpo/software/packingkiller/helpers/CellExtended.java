package gpo.software.packingkiller.helpers;


import org.apache.poi.ss.usermodel.Cell;

/**
 * Created by gpowork on 14/02/2017.
 */
public class CellExtended {

    private final Cell cell;
    public int mergedColumns = 1;
    public int mergedRows = 1;
    public boolean isMerged = false;
    public String text;

    public CellExtended(Cell cell, String text) {
        this.cell = cell;
        if (text != null && !text.isEmpty())
            this.text = text;
        else
            switch(cell.getCellType()) {
                case Cell.CELL_TYPE_BLANK:
                    text = cell.getStringCellValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    text = cell.getNumericCellValue() + "";
                    break;
                case Cell.CELL_TYPE_FORMULA:
                    text = cell.getCellFormula();
                    break;
                case Cell.CELL_TYPE_STRING:
                    text = String.valueOf(cell.getRichStringCellValue());
                    break;
                case Cell.CELL_TYPE_ERROR:
                    text = "";
                    break;
                default:
                    text = cell.getStringCellValue();
            }
    }

    public Cell getCell() {
        return cell;
    }
}
