package gpo.software.packingkiller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by gpowork on 11/02/2017.
 */
public class MainRunner extends Application {

    public static Stage globalStage;

    @Override
    public void start(final Stage stage) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("/main_layout.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Packing Killer");
        stage.setMaximized(true);
        stage.setScene(scene);
        stage.show();
        globalStage = stage;

    }

    public static void main(String... args) {
        launch(args);
    }
}
