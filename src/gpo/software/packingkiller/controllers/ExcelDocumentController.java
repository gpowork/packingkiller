package gpo.software.packingkiller.controllers;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

/**
 * Created by gpowork on 14/02/2017.
 */
public class ExcelDocumentController {
    @FXML private VBox mainContainer;

    public VBox getMainContainer() {
        return mainContainer;
    }
}
