package gpo.software.packingkiller.controllers;

import gpo.software.packingkiller.services.ExcelWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Created by gpowork on 14/02/2017.
 */
public class ProjectDialogController implements Initializable {


    @FXML private Button closeBtn;
    @FXML private ListView<HBox> selectedFiles;
    private final static FileChooser fileChooser = new FileChooser();
    private List<ExcelWindow> excelList = new ArrayList<>();

    private ObservableList<HBox> list = FXCollections.observableArrayList();

    public void addTextField(HBox row) {
        list.add(row);
    }

    public void cancel(ActionEvent event) {
        Stage stage = (Stage) closeBtn.getScene().getWindow();
        stage.close();
    }

    public void addNew(ActionEvent event) {
       openFileDialog((Stage) closeBtn.getScene().getWindow());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selectedFiles.setItems(list);
    }


    public void openFileDialog(Window owner) {
        List<File> excelFiles = fileChooser.showOpenMultipleDialog(owner);
        if (excelFiles != null && !excelFiles.isEmpty()) {
            for(File excelFile: excelFiles) {
                if (excelFile != null && excelFile.exists()) {
                    final ExcelWindow excelWindow = new ExcelWindow(excelFile.getAbsolutePath(), excelFile.getName());
                    excelList.add(excelWindow);
                    HBox row = new HBox();
                    Button mergeBtn = new Button("^");
                    Button openWindow = new Button(">");
                    TextField companyName = new TextField();
                    companyName.setPromptText("Company Name");
                    companyName.setText(excelFile.getName());
                    row.getChildren().add(mergeBtn);
                    row.getChildren().add(companyName);
                    row.getChildren().add(openWindow);
                    addTextField(row);
                    mergeBtn.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            appendExcelToOutput(excelWindow);
                        }
                    });
                    try {
                        excelWindow.show();
                    } catch (IOException e) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("File Open Error");
                        alert.setHeaderText("Can not open this file. Convert to MS Excel 2004 format and try again.");
                        alert.showAndWait();
                        System.out.println(excelFile.getAbsolutePath());
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void appendExcelToOutput(ExcelWindow exc) {
        String[] selected = exc.getSelected();
        int dataRowIndex = exc.getFirstDataRowIndex();
        System.out.println("dataRowIndex: " + dataRowIndex);

    }

    private static final int END_HEADER_ROW = 5;
    private static final int END_HEADER_COL = 22;

//    private void copyHeaderFromTemplate(WritableSheet wsheet) throws WriteException {
//        WritableWorkbook fromBook = null;
//        try {
//            fromBook = Workbook.createWorkbook(new File("temp.xls"), MainWindowController.defaultResultExcel.getWorkbook());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        WritableSheet sh = fromBook.getSheet(0);
//        Map<CellFormat, WritableCellFormat> definedFormats = new HashMap<>();
//        for (int col = 0; col < END_HEADER_COL; col++) {
////            wsheet.setColumnView(col, sh.getColumnView(col));
//            for (int row = 0; row < END_HEADER_ROW; row++) {
////                if (col == 0)
////                    wsheet.setRowView(row, sh.getRowView(row));
////                WritableCell readCell = sh.getWritableCell(col, row);
//////                Label label = new Label(col, row, readCell.getContents());
////                WritableCell newCell = readCell.copyTo(col, row);
////                CellFormat readFormat = readCell.getCellFormat();
////                if (readFormat != null) {
////                    if (!definedFormats.containsKey(readFormat)) {
////                        definedFormats.put(readFormat, new WritableCellFormat(readFormat));
////                    }
////                    newCell.setCellFormat(definedFormats.get(readFormat));
////                }
//                WritableCell newCell = new WritableCell() {
//                }
//                wsheet.addCell(newCell);
//            }
//        }
//    }

    private void copyFooterFromTemplate() {

    }

    private void copyBodyFromTemplate() {

    }

//    private void exportToExcel() throws IOException {
//        WritableWorkbook wkbook = Workbook.createWorkbook(new File("/Users/gpowork/Downloads/text.xls"));
//        WritableSheet wsheet = wkbook.createSheet("Sheet 1", 0);
//
//        try {
//            copyHeaderFromTemplate(wsheet);
//        } catch (WriteException e) {
//            e.printStackTrace();
//        }
//
//        wkbook.write();
//        try {
//            wkbook.close();
//        } catch (WriteException e) {
//            e.printStackTrace();
//        }
//    }
}
