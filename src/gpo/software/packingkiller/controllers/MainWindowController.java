package gpo.software.packingkiller.controllers;

import gpo.software.packingkiller.services.ExcelWindow;
import gpo.software.packingkiller.services.ProjectDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

import java.io.IOException;

/**
 * Created by gpowork on 12/02/2017.
 */
public class MainWindowController{

    public static ExcelWindow defaultResultExcel = null;

    @FXML private VBox mainContent;

    @FXML
    public void projectDialog(final ActionEvent event) {
        ProjectDialog projectDialog = new ProjectDialog();
        projectDialog.show(event);
        if (defaultResultExcel == null) {
            defaultResultExcel = new ExcelWindow(ExcelWindow.TEMPLATE_PATH, "Result Excel");
        }
        try {
            defaultResultExcel.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void exit(final ActionEvent event) {
        System.exit(0);
    }

}
