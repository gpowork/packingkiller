package gpo.software.packingkiller.services;

import gpo.software.packingkiller.controllers.ProjectDialogController;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by gpowork on 14/02/2017.
 */
public class ProjectDialog {
    private Stage stage;
    private ProjectDialogController projectDialogController;
    public ProjectDialog() {
        stage = new Stage();
        Parent root = null;
        try {
            FXMLLoader loader = new FXMLLoader(ProjectDialog.class.getResource("/project_dialog.fxml"));
            root = (Parent)loader.load();
            projectDialogController = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(new Scene(root));
        stage.setTitle("Modal");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setWidth(300.0);
        stage.setHeight(200.0);
        stage.setX(10.0);
        stage.setY(100.0);
        stage.setResizable(true);
    }

    public void show(ActionEvent event) {
        stage.show();
    }

}


