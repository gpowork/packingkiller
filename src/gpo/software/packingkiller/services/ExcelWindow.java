package gpo.software.packingkiller.services;

import gpo.software.packingkiller.MainRunner;
import gpo.software.packingkiller.controllers.ExcelDocumentController;
import gpo.software.packingkiller.helpers.CellExtended;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gpowork on 13/02/2017.
 */
public class ExcelWindow {

    private Stage stage;
    private String pathToExcel;
    private HSSFWorkbook workbook = null;
    private HSSFSheet sheet = null;
    private String title;
    private WebView browser = new WebView();
    private WebEngine engine = browser.getEngine();
    private static int TITLE_ROW_1 = 4;
    public static List<String> titleList = null;
    private boolean isDefault = false;

    private ExcelDocumentController excelDocumentController;

    public static final String TEMPLATE_PATH = "res/xls/template01.xls";

    public ExcelWindow(String pathToExcel, String title) {
        this.stage = new Stage();
        Parent root = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/excel_view.fxml"));
            root = (Parent)loader.load();
            excelDocumentController = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(new Scene(root));
        stage.setTitle(title);
        stage.initModality(Modality.WINDOW_MODAL);
        float mainWindowWidth = (float) MainRunner.globalStage.getWidth();
        float width = (float) mainWindowWidth * 2 / 3;
        stage.setWidth(width);
        stage.setX(mainWindowWidth - width);
        stage.setY(100.0);
        stage.setHeight(500.0);
        stage.setResizable(true);

        this.pathToExcel = pathToExcel;
        this.title = title;
    }

    private void init() throws IOException {
        File inputWorkBook = new File(pathToExcel);
        if (inputWorkBook != null && inputWorkBook.exists()) {
            workbook = new HSSFWorkbook(new FileInputStream(inputWorkBook));
            sheet = workbook.getSheetAt(0);
            if (TEMPLATE_PATH.equals(pathToExcel)) {
                isDefault = true;
                titleList = new ArrayList<>();
                for (Cell c: sheet.getRow(TITLE_ROW_1)) {
                    String title = c.getStringCellValue();
                    Cell cellUnder = sheet.getRow(TITLE_ROW_1 + 1).getCell(c.getColumnIndex());
                    if (!cellUnder.getStringCellValue().isEmpty())
                        title = title + "." + cellUnder.getStringCellValue();
                    titleList.add(title);
                }
            }
        }
        engine.setJavaScriptEnabled(true);
    }

    public void show() throws IOException {
        if (workbook == null || sheet == null) {
            init();
            engine.setUserStyleSheetLocation(ExcelWindow.class.getResource("/styles/excel_styles.css").toExternalForm());
            excelDocumentController.getMainContainer().getChildren().add(browser);
        }
        stage.show();
        List<String> list = null;
        try {
            list = Files.readAllLines(Paths.get(getClass().getResource("/js/excel.js").toURI()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        StringBuilder scripts = new StringBuilder();
        if (list != null && !list.isEmpty()) {
            scripts.append("<script type=\"text/javascript\">");
            for (String s: list)
                scripts.append(s);
            scripts.append("</script>");
        }
        engine.loadContent("<html><head>" + scripts.toString() + "</head><body>" + renderGridStr() + "</body></html>");
    }


    private Map<String, Drawing> imagesMap = new HashMap<String, Drawing>();

    private CellExtended[][] parseExcel() {

        int rows = sheet.getLastRowNum() + 1;
        if (rows > 1000)
            rows = 1000;

        int columns = sheet.getRow(0).getLastCellNum() + 1;
        if (columns > 30)
            columns = 30;

        CellExtended[][] table = new CellExtended[rows][columns];
        // Preparing table
        System.out.println("table: " + rows +"=" + table.length +", " +columns+"="+table[0].length);

//        if (sheet.getDrawingPatriarch() != null) {
//            for (Drawing img : sheet.getDrawingPatriarch()) {
//                if (img != null) {
//                    img.
//                    String key = ((int)img. + "-" + (int)img.getColumn());
//                    imagesMap.put(key, img);
//                }
//            }
//        }
        for(int i = 0; i < table.length; i++)
            for (int j = 0; j < table[i].length; j++) {
                Cell cell = sheet.getRow(i).getCell(j);
                if (cell != null) {
                    if (imagesMap.containsKey(i + "-" + j)) {
    //                    try {
    //                        table[i][j] = new CellExtended(cell, "<img class='img-preview' src='data:image/png;base64," + DatatypeConverter.printBase64Binary(imagesMap.get(i + "-" + j).getImageBytes())+ "'/>");
    //                    } catch (IOException e) {
    //                        e.printStackTrace();
                            table[i][j] = new CellExtended(cell, null);
    //                    }
                    } else {
                        table[i][j] = new CellExtended(cell, null);
                    }
                }
            }

        // Mark merged regions

        for (int k = 0; k < sheet.getNumMergedRegions(); k++) {
            CellRangeAddress r = sheet.getMergedRegion(k);
            Cell start = sheet.getRow(r.getFirstRow()).getCell(r.getFirstColumn());
            Cell end = sheet.getRow(r.getLastRow()).getCell(r.getLastColumn());
            CellExtended c = table[r.getFirstRow()][r.getFirstColumn()];
            c.mergedColumns = r.getLastColumn() - r.getFirstColumn() + 1;
            c.mergedRows = r.getLastRow() - r.getFirstRow() + 1;
            for(int i = r.getFirstRow(); i <= r.getLastRow(); i++) {
                for (int j = r.getFirstColumn(); j <= r.getLastColumn(); j++) {
                    if (i != r.getFirstRow() || j != r.getFirstColumn()) {
                        table[i][j].isMerged = true;
                    }
                }
            }
        }
        return table;
    }

    private static String getSelect(int selected) {
        StringBuilder str = new StringBuilder();
        str.append("<select class='title-select'>");
        for(int i = 0; i < titleList.size(); i++) {
            str.append("<option value='")
                    .append(i + 1)
                    .append("'");
            if (i == selected)
                str.append(" selected");
            str.append(">")
                    .append(titleList.get(i))
                    .append("</option>");
        }
        str.append("</select>");
        return str.toString();
    }

    private String renderGridStr() {
        StringBuilder str = new StringBuilder();
        str.append("<table id='mainTable' cellspacing='0'>");
        CellExtended[][] table = parseExcel();
        StringBuilder titleFirst = new StringBuilder();
        StringBuilder titleSecond = new StringBuilder();
        titleFirst.append("<tr><th>N</th>");
        if (!isDefault)
            titleSecond.append("<tr><th>&nbsp;</th>");
        for (int j = 0; j < table[0].length; j++) {
            titleFirst.append("<td>")
                    .append(String.valueOf((char) (j + 65)))
                    .append("</td>");
            if (!isDefault)
                titleSecond.append("<td>")
                        .append(getSelect(j))
                        .append("</td>");
        }
        titleFirst.append("</tr>");
        titleSecond.append("</tr>");
        str.append(titleFirst);
        if (!isDefault)
            str.append(titleSecond);
        for (int i = 0; i < table.length; i++) {
            str.append("<tr><th><a href='#' onclick='markStartRow(")
                    .append(i)
                    .append(");'>")
                    .append(i + 1)
                    .append("</a></th>");
            for (int j = 0; j < table[i].length; j++) {
                CellExtended c = table[i][j];
                if (c != null && !c.isMerged) {
                    String colSpan = "";
                    String rowSpan = "";
                    if (c.mergedColumns > 1)
                        colSpan = " colspan='" + c.mergedColumns + "'";
                    if (c.mergedRows > 1)
                        rowSpan = " rowspan='" + c.mergedRows + "'";
                    String content = "&nbsp;";
                    if (c.text != null && !c.text.isEmpty())
                        content = c.text;
                    str.append("<td" + colSpan + rowSpan + ">")
                            .append(content)
                            .append("</td>");
                }
            }
            str.append("</tr>");
        }
        str.append("</table>");
        return str.toString();
    }

    public String[] getSelected() {
        if (engine.isJavaScriptEnabled()) {
            JSObject win = (JSObject)engine.executeScript("window");
            String selected = win.eval("getSelected()").toString();
            if (selected != null) {
                return selected.split(",");
            }
        }
        return null;
    }

    public int getFirstDataRowIndex() {
        for (int i = 0; i < sheet.getLastRowNum(); i++) {
            int score = 0;
            for (int j = 0; j < sheet.getRow(i).getLastCellNum(); j++) {
                Cell cell = sheet.getRow(i).getCell(j);
                if (cell != null && cell.getStringCellValue() != null) {
                    String content = cell.getStringCellValue().toLowerCase();
                    if (content.indexOf("total") > -1)
                        score++;
                    if (content.indexOf("ctn") > -1)
                        score++;
                    if (content.indexOf("qty") > -1)
                        score++;
                    if (content.indexOf("g.w.") > -1)
                        score++;
                    if (content.indexOf("n.w.") > -1)
                        score++;
                    if (content.indexOf("color") > -1)
                        score++;
                    if (content.indexOf("picture") > -1)
                        score++;
                }
            }
            if (score > 3)
                return i + 1;
        }
        return 0;
    }

    public HSSFSheet getSheet() {
        return sheet;
    }

    public HSSFWorkbook getWorkbook() {
        return workbook;
    }

}
